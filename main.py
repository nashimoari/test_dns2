from _datetime import datetime


def allcharacterssame(s):
    n = len(s)
    for i in range(1, n):
        if s[i] != s[0]:
            return False
    return True


# Собираем статистику по количеству для всех встречающихся средних
def get_stat_by_avg():
    out = {}
    for x in range(0, 100):
        a = [int(i) for i in str(x)]
        avg = sum(a) / 2
        if str(avg) in out:
            out[str(avg)] += 1
        else:
            out[str(avg)] = 1
    return out


def find_lucky_fast():
    out = {}

    # Предварительно соберем информацию сколько у нас встречается средних на интервале внутри группы
    avg_all = get_stat_by_avg()

    # Перебираем рулоны
    for x in range(0, 10):

        cnt_lucky_at_rulon = 0  # Количество счастливых билетов в рулоне

        # Расчитываем количество счастливых билетов внутри одного рулона
        for b in range(0, 10):

            # Находим среднее для первой группы
            avg_sum_grp1 = (x + b) / 2

            # берем количество встречающихся средних на всей длине группы
            cnt_avg = avg_all[str(avg_sum_grp1)]

            # Всего 3 группы поэтому чтобы получить количество счастливых билетов нам нужно возвести в квадрат
            # количество встречающихся средних для среднего которое мы получили в первой группе
            cnt_lucky_at_rulon += pow(cnt_avg, 2)

        # и нужно вычесть 1 чтобы исключить тот случай когда все числа в одном рулоне равны. Такая ситуация в одном
        # рулоне будет встречаться 1 раз
        cnt_lucky_at_rulon -= 1
        out['rulon_' + str(x)] = cnt_lucky_at_rulon
    return out


def find_lucky_by_bruteforce():
    out = {}

    # Перебираем рулоны
    for x in range(0, 10):
        cnt_lucky_at_rulon = 0  # Количество счастливых билетов в рулоне

        # Перебираем рулон
        for n in range(0, 99999):
            ticket_number = str(n)
            while len(ticket_number) < 5:
                ticket_number = '0' + ticket_number

            ticket_number = str(x) + ticket_number

            # скипаем если все числа равны
            if allcharacterssame(ticket_number):
                continue

            a = [int(i) for i in str(ticket_number)]

            # Расчитываем среднее для каждой группы
            avg1 = (a[0] + a[1]) / 2
            avg2 = (a[2] + a[3]) / 2
            avg3 = (a[4] + a[5]) / 2

            if avg1 == avg2 == avg3:
                cnt_lucky_at_rulon += 1

        out['rulon_' + str(x)] = cnt_lucky_at_rulon

    return out



start_time = datetime.now()
res = find_lucky_fast()

print('Поиск счастливых билет быстрым способом:')
print('Время выполнения: ' + str(datetime.now() - start_time))
print('распределение счастливых билетов по рулонам:')
print(res)

start_time = datetime.now()
res = find_lucky_by_bruteforce()
print('=============================================')
print('Поиск счастливых билет перебором:')
print('Время выполнения: ' + str(datetime.now() - start_time))
print('распределение счастливых билетов по рулонам:')
print(res)
